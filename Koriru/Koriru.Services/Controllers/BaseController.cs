﻿namespace Koriru.Services.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class BaseController<T> : Controller
    {
        protected readonly IStringLocalizer<T> _localizer;
        protected readonly ILogger<T> _log;
        public BaseController(IStringLocalizer<T> localizer, ILogger<T> log)
        {
            _localizer = localizer;
            _log = log;
        }
    }
}
