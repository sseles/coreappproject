﻿using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Koriru.Services.Controllers
{
    public class BaseApiController<T> : BaseController<T>
    {
        public BaseApiController(IStringLocalizer<T> localizer, ILogger<T> log) : base(localizer, log)
        {
        }
    }
}
