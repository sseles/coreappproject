﻿namespace Koriru.Services.AppStart
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.AspNetCore.Routing.Constraints;
    public static class RouteConfig
    {
        public static void RouteBuilder(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "cultureRoute",
                template: "{culture}/{controller}/{action}/{id?}",
                defaults: new { controller = "Home", action = "Index" },
                constraints: new
                {
                    culture = new RegexRouteConstraint("^[a-z]{2}(?:-[A-Z]{2})?$")
                });

                routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
