﻿namespace Koriru.Services.AppStart
{
    using Koriru.Core.Providers;
    using Koriru.Data.Models.Identity;
    using Koriru.Data.Statics;
    using Koriru.DataAccessLevel.Contexts;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddViewLocalization()
                .AddDataAnnotationsLocalization();

            services.AddLocalization(options => { options.ResourcesPath = "Resources"; });

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddSession();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = Localization.DefaultCultureInfo;
                options.SupportedCultures = Localization.SupportedCultures;
                options.SupportedUICultures = Localization.SupportedCultures;
                options.RequestCultureProviders.Insert(0, new UrlRequestCultureProvider());
            });
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(Paths.LogFolder);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            app.UseDefaultFiles();
            app.UseSession();
            app.UseStaticFiles();
            app.UseIdentity();

            RouteConfig.RouteBuilder(app);
        }
    }
}
