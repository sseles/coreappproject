﻿namespace Koriru.DataAccessLevel.Contexts
{
    using Koriru.Data.Models.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

    }
}
