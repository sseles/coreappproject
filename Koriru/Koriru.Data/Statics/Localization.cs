﻿namespace Koriru.Data.Statics
{
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.AspNetCore.Localization;
    public static class Localization
    {
        #region Properties
        public static string ApplicationName
        {
            get
            {
                return applicationName;
            }
        }
        public static RequestCulture DefaultCultureInfo
        {
            get
            {
                return defaultCultureInfo;
            }
        }
        public static List<CultureInfo> SupportedCultures
        {
            get
            {
                return supportedCultures;
            }
        }

        #endregion
        #region Initializers
        private const string applicationName = "myApp";
        private static readonly RequestCulture defaultCultureInfo = new RequestCulture("en");
        private static readonly List<CultureInfo> supportedCultures = new List<CultureInfo>()
        {
            new CultureInfo("en")
        };
        #endregion
    }
}
