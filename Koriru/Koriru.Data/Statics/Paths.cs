﻿namespace Koriru.Data.Statics
{
    public static class Paths
    {
        public static string LogFolder
        {
            get
            {
                return logFolder;
            }
        }

        private const string logFolder = "Logs/myApp-{Date}.txt";
    }
}
