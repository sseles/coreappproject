﻿namespace Koriru.Controllers
{
    using System;
    using Koriru.Services.Controllers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;

    public class HomeController : BaseController<HomeController>
    {
        public HomeController(IStringLocalizer<HomeController> localizer, ILogger<HomeController> log) : base(localizer, log)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = _localizer["Title"];
            return View();
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}
